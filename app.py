from flask import Flask, jsonify, abort, request
import db


app = Flask(__name__)


@app.route('/api/v1.0/peoples', methods=['GET'])
def get_peoples():
    return jsonify({'peoples': db.read()})


@app.route('/api/v1.0/people/<int:id>', methods=['GET'])
def get_people_by_id(id):
    query = db.read(id)
    if query:
        return jsonify({query[0]['name']: query})
    abort(404, 'Nenhuma pessoa com essa ID')


@app.route('/api/v1.0/peoples', methods=['POST'])
def insert_new_people():
    people = request.json
    required = (
        'name' in people,
        'id' in people,
        'age' in people
    )

    if people and all(required):
        db.insert(people)
    else:
        abort(400)

    return "Inserido!", 201


if __name__ == '__main__':
    app.run(debug=True)
