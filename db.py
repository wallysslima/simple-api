import json


def load() -> list:
    with open('db.json', mode='r', encoding='utf-8') as db:
        peoples = json.loads(db.read())

    return peoples


def insert(people: dict):
    peoples = load()
    with open('db.json', mode='w', encoding='utf-8') as db:
        peoples.append(people)
        db.write(json.dumps(peoples))


def delete():
    pass


def update():
    pass


def read(id: str = '') -> dict:
    peoples = load()
    if id:
        return [people for people in peoples if id == people['id']]
    return peoples
